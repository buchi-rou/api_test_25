import requests
from requests.auth import HTTPBasicAuth
import random
from jsonpath import jsonpath

from module_apis.loginRegisterApis import LoginRegisterApis
from db_datas.DBUsersData import UserInfoDB




class TestRegisterLogin:

    # 场景一：新用户注册
    # def test_new_user_register(self):
    #     # 调用注册接口，输入新账号实现用户注册
    #     url = "https://mall.deyunce.com/api/account/register"
    #     params_data = {"mobile": "18900000021", "password": "1111111.", "code": "", "client": 5}
    #     res = requests.post(url, json=params_data, auth=HTTPBasicAuth("deyunce", "828123"))
    #     # 获取接口的响应码
    #     status_code = res.status_code
    #     # 获取接口的响应体
    #     response = res.json()
    #     msg = response.get("msg")
    #     # 进行断言
    #     assert status_code ==  200
    #     assert msg == '注册成功'

    # 场景二：注册过的账号去注册
    def test_old_mobile_for_register(self):
        # 调用注册接口，输入新账号实现用户注册
        url = "https://mall.deyunce.com/api/account/register"
        params_data = {"mobile": "18900000021", "password": "1111111.", "code": "", "client": 5}
        res = requests.post(url, json=params_data, auth=HTTPBasicAuth("deyunce", "828123"))
        # 获取接口的响应码
        status_code = res.status_code
        # 获取接口的响应体
        response = res.json()
        msg = response.get("msg")
        # 进行断言
        assert status_code == 200
        assert msg == '此手机号已被使用'

    # 场景三：新用户注册
    def test_new_user_register(self):
        # 调用注册接口实现新用户注册
        '''
        通过随机数生成一个新的号码，在调用注册接口前获取数据库中已注册的用户，判断该号码是否存在，如果存在，继续生成新号码，
        直到数据库中不存在该号码，再执行注册，注册完成，再次判断该号码是否存在数据库中
        '''

        login_register_apis = LoginRegisterApis()
        # 通过随机数动态生成一个用于注册的手机号 177xxxxxxxx
        new_user_mobile = "177" + str(random.randint(10000000, 99999999))
        password = "111111."
        # 判断生成的手机号是否已存在与数据库中
        user_datas = UserInfoDB()
        user_mobiles = user_datas.get_user_mobiles()
        # 当生成的新号码已经在数据库中时，再执行动态生成号码的代码
        while (new_user_mobile in user_mobiles):
            new_user_mobile = "177" + str(random.randint(10000000, 99999999))
        print(f"注册的新用户账号是：{new_user_mobile}")
        res = login_register_apis.register_api(mobile= new_user_mobile, password = password)
        status_code = res.get("status_code")
        register_response = res.json()
        msg = jsonpath(register_response, "$..msg")[0]
        assert status_code == 200
        assert msg == "注册成功"
        # 验证数据库中的数据，user表中是否已经插入了该条数据
        # 数据库中获取所有已注册的用户
        user_mobiles = user_datas.get_user_mobiles()  # 获取所有已注册用户的mobile列表
        assert new_user_mobile in user_mobiles

    # 场景四：测试后端用户登录
    def test_user_login(self):
        login_register_apis = LoginRegisterApis()
        res = login_register_apis.login_api(account="15200000001", password='1111111.')
        status_code = res.status_code
        register_response = res.json()
        token = jsonpath(register_response, "$..token")[0]
        assert status_code == 200
        assert token != ""
        # 还可以对接口返回的用户数据结合DB进行断言




