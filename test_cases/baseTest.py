from jsonpath import jsonpath
from common.log import Logger


logging = Logger(__name__).get_logger()
class BaseTest:

    """
    封装测试用例中常用的工具方法
    """

    def get_test_datas(self):
        pass


    # 封装一个jsonpath的方法
    def json_res(self, res, exp, index=0):
        json_res = jsonpath(res, exp)
        logging.info(f"jsonpath 匹配到的结果是{json_res}")
        if json_res and index is not None: # 当调用时未指定index的值时，默认取匹配到的第一个返回
            return json_res[index]
        elif json_res and index is None:  # 当调用时index=None 时会返回匹配到的全部
            return json_res
        else:
            raise TypeError("jasonpath 未匹配到数据")