import pytest
import requests
from requests.auth import HTTPBasicAuth
from jsonpath import jsonpath

@pytest.fixture()
def get_token():
    url = "https://mall.deyunce.com/api/account/login"
    params_data = {"account": "15200000001", "password": "111111.", "client": 5}
    res = requests.post(url, json=params_data, auth=HTTPBasicAuth("deyunce", "828123"))
    response = res.json() # 通过jsonpath来获取对应的指定key的值
    token_list = jsonpath(response, "$..token")
    if token_list:
        token = token_list[0]
        yield token
    else:
        raise TypeError("jsonpath 未匹配到token,请检查jsonpath表达式")