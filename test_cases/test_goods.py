import requests
from requests.auth import HTTPBasicAuth
from jsonpath import jsonpath

from module_apis.goodsApis import GoodsApis
from db_datas.DBgoodsData import DBGoodsDatas
from test_cases.baseTest import BaseTest

class TestGoods(BaseTest):

    goods_apis = GoodsApis()

    def test_add_goods_in_cart(self, get_token):
        url = "https://mall.deyunce.com/api/cart/add"
        params_data = {"item_id":1178,"goods_num":1}
        header = {"Token":get_token}  #49e43b9aaddbda9b8649be1d76cdc5b7
        res = requests.post(url, headers=header, json=params_data, auth=HTTPBasicAuth("deyunce", "828123"))
        # 获取接口的响应码
        status_code = res.status_code
        # 获取接口的响应体
        response = res.json()
        msg = response.get("msg")
        # 进行断言
        assert status_code == 200
        assert msg == "加入成功"

    # 练习：编写测试用例实现对商品搜索接口的测试
    def test_search_goods(self, get_token):
        goods_apis = GoodsApis()
        res = goods_apis.search_goods_api(token=get_token, goods="铅笔")
        # 获取status_code
        status_code = res.get("status_code")
        assert status_code == 200
        # 获取接口所有name的值
        name_list = jsonpath(res, "$..name")
        for goods_name in name_list:
            assert "文具" in goods_name


    # 场景一：测试商品的收藏
    def test_collect_goods(self, get_token):
        # 判断该商品是否收藏过
        # 获取指定用户已收藏的商品
        db_goods_datas = DBGoodsDatas()
        collect_goods_list = db_goods_datas.get_collect_goods_by_userID(user_id=201)
        test_goods_id = 750
        if test_goods_id in collect_goods_list:
            # 如果goods_id=750在收藏列表中，则首先去取消该商品的收藏
            self.goods_apis.collect_goods_api(get_token, is_collect=0, goods_id=test_goods_id)
        # 收藏该商品
        res = self.goods_apis.collect_goods_api(get_token, is_collect=1, goods_id=test_goods_id)
        # 接口层面进行断言
        status_code = res.get("status_code")
        msg = self.json_res(res, "$..msg")
        assert status_code == 200
        assert msg == "获取成功"
        # 数据库层面进行断言
        collect_goods_list = db_goods_datas.get_collect_goods_by_userID(user_id=201)
        assert test_goods_id in collect_goods_list



    # 场景二：测试商品的取消
    def test_cancel_collect_goods(self, get_token):
        # 判断该商品是否收藏过
        # 获取指定用户已收藏的商品
        db_goods_datas = DBGoodsDatas()
        collect_goods_list = db_goods_datas.get_collect_goods_by_userID(user_id=201)
        test_goods_id = 750
        if test_goods_id not in collect_goods_list:
            # 如果商品没有收藏，首先收藏该商品
            self.goods_apis.collect_goods_api(get_token, is_collect=1, goods_id=test_goods_id)
        # 取消收藏该商品
        res = self.goods_apis.collect_goods_api(get_token, is_collect=0, goods_id=test_goods_id)
        # 接口层面进行断言
        status_code = res.get("status_code")
        msg = self.json_res(res, "$..msg")
        assert status_code == 200
        assert msg == "获取成功"
        # 数据库层面进行断言
        collect_goods_list = db_goods_datas.get_collect_goods_by_userID(user_id=201)
        assert test_goods_id not in collect_goods_list
