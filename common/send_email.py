import smtplib
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# 1.设置服务器信息
# mail_host = "smtp.qq.com"
# mail_user = "372109913"
# mail_pass = "ualtbmajjpmmbgjj"
# sender = '372109913@qq.com'
# receivers = ['372109913@qq.com']
# # 2.构建邮件的主体
# # 构建纯文本邮件主体
# # ms = "本次测似通过，测试报告地址：https://xxxxx.jjjj.com"
# # message = MIMEText(ms,'plain','UTF-8')
# # message['Subject'] = '一封来自测试的信'  # 邮件标题
# # message['From'] = Header("Jie的发送邮箱","utf-8") # 邮件主体中发送者名称
# # message['To'] = Header("Jie的接收邮箱","utf-8") # 邮件主体中接收者名称
# # 构建一个混合主体（既能发送字符串，又带有附件）
# message = MIMEMultipart() # 生成包含多个部分的邮件主体对象
# message['Subject'] = '自动化测试报告'
# message['From'] = Header("思源的发送邮箱","utf-8")
# message['To'] = Header("Jie的接收邮箱","utf-8")
# ms_body = MIMEText('本次测似通过，测试报告地址：https://xxxxx.jjjj.com','plain', 'UTF-8')
# file_path = r"C:\Users\37210\Desktop\自动化测试.txt"
# att_body = open(file_path, 'rb')  # 以二进制的形式打开文件
# att = MIMEApplication(att_body.read()) # 导入附件
# att_body.close()
# att.add_header('Content-Disposition','attachment',filename='自动化测试.txt')
# message.attach(ms_body)
# message.attach(att)
#
# # 3.进行登录发送邮件
# smtpobj = None
# try:
#     smtpobj = smtplib.SMTP()
#     smtpobj.connect(mail_host,25)   # 创建与mail_host的连接
#     smtpobj.login(mail_user,mail_pass)  # 登录邮箱
#     smtpobj.sendmail(sender,receivers,message.as_string())
#     print('success')
# except Exception as e:
#     print(f'error: {e}')
# finally:
#     if smtpobj:
#         smtpobj.quit()

# 封装成SendEmail类

import smtplib
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


class SendEmail:

    # 初始化服务器信息
    def __init__(self,mail_host, mail_user, mail_pass, sender, receives):
        self.mail_host = mail_host
        self.mail_user = mail_user
        self.mail_pass = mail_pass
        self.sender = sender
        self.receivers = receives

    # 以文本的形式发送邮件
    def make_email_by_text(self,context, subject, from_address, to_address):
        message = MIMEText(context, 'plain', 'UTF-8')
        message['Subject'] = subject  # 邮件标题
        message['From'] = Header(from_address, "utf-8")  # 邮件主体中发送者名称
        message['To'] = Header(to_address, "utf-8")  # 邮件主体中接收者名称
        self.send_email(message)

     # 以文本和附件的形式发送邮件
    def make_email_by_att(self,content, file_path,subject, from_address, to_address):
        message = MIMEMultipart()
        message['Subject'] = subject  # 邮件标题
        message['From'] = Header(from_address, "utf-8")  # 邮件主体中发送者名称
        message['To'] = Header(to_address, "utf-8")  # 邮件主体中接收者名称
        body = MIMEText(content,'plain','utf-8')
        message.attach(body)
        att_body = open(file_path, 'rb') # 以二进制的格式打开附件
        att = MIMEApplication(att_body.read()) # 导入附件
        att_body.close()
        att.add_header('Content-Disposition','attachment',filename='allure测试报告.zip') # 添加附件名称
        message.attach(att)
        self.send_email(message)

    # 登录并进行发送
    def send_email(self,message):

        # 进行登录发送
        try:
            smtpobj = smtplib.SMTP()
            smtpobj.connect(self.mail_host,25)
            smtpobj.login(self.mail_user,self.mail_pass)
            smtpobj.sendmail(self.sender,self.receivers,message.as_string())
            smtpobj.quit()
            print('success')
        except Exception as e:
            print(f'error: {e}')
            raise e


if __name__ == '__main__':
    pass




