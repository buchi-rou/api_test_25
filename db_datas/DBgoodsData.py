from db_datas.BaseDB import BaseDB

class DBGoodsDatas:


    # 通过user_id查询数据库中该用户收藏的商品，返回goods_id列表
    def get_collect_goods_by_userID(self, user_id) -> list:
        db = BaseDB()
        sql = f"SELECT goods_id FROM `ls_goods_collect` where user_id = {user_id};"
        res = db("select", sql)
        db(connect=False)
        collect_goods_list = []
        for goods_id_dict in res:
            goods_id = goods_id_dict.get("goods_id")
            collect_goods_list.append(goods_id)
        return collect_goods_list


if __name__ == '__main__':
    db_goods_datas = DBGoodsDatas()
    res = db_goods_datas.get_collect_goods_by_userID(user_id=201)
    print(res)