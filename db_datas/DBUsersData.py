from db_datas.BaseDB import BaseDB
"""
该模块用于封装在测试中需要用到的获取数据库用户数据的所有方法
"""
class UserInfoDB:

    # 获取用户的所有手机号
    def get_user_mobiles(self):
        db = BaseDB()
        # 查询ls_user表中的所有用户的电话
        sql = "select mobile from ls_user"
        db_users = db("select", sql)
        # print(db_users)
        mobiles_list = []
        for mobile_dict in db_users:
            mobile = mobile_dict.get("mobile")
            mobiles_list.append(mobile)
        db(connect=False)
        # print(mobiles_list)
        return mobiles_list

    # 获取用户的nickname
    def get_user_nicename(self):
        db = BaseDB()
        # 查询ls_user表中的所有用户昵称
        sql = "select nickname from ls_user"
        db_users = db("select", sql)
        # print(db_users)
        nickname_list = []
        for nickname_dict in db_users:
            nickname = nickname_dict.get("nickname")
            nickname_list.append(nickname)
        db(connect=False)
        # print(nickname_list)
        return nickname_list


