from module_apis.BaseApi import BaseApi


class GoodsApis(BaseApi):
    """
    类下面封装最小粒度的接口请求，并把整个res，即接口响应对象返回出去，这里就好比造乐高的零件，讲究零件的通用化，即能够在不同用例中被调用
    在测试用例中调用下面封装的方法，通过组合调用的形式形成业务场景，即我们的测试场景，就好比把乐高零件组装成对应的模型
    """

    def __init__(self):
        self.yaml_path = BaseApi.get_path(r"/api_datas/goodsApis.yaml")

    # 商品详情接口
    def goods_detail_api(self):
        pass

    # 商品搜索接口
    def search_goods_api(self, token, goods):
        res = self.run_requests(self.yaml_path, "search_goods_api", token=token, goods_name=goods)
        return res


    # 收藏/或取消商品接口
    def collect_goods_api(self,token, is_collect, goods_id):
        res = self.run_requests(self.yaml_path, "collect_goods_api", token=token, is_collect=is_collect, goods_id=goods_id)
        return res