import requests
from requests.auth import HTTPBasicAuth

from module_apis.BaseApi import BaseApi


class LoginRegisterApis(BaseApi):

    """
    封装关于所有注册/登录接口的业务接口，把业务接口封装成单个方法
    """

    def __init__(self):
        self.yaml_path = BaseApi.get_path(r"/api_datas/loginRegisterApis.yaml")


    # 登录接口
    def login_api(self, account, password):
        res = self.run_requests(self.yaml_path, "login_api")
        return res


    # 注册接口
    def register_api(self, mobile, password):
        res = self.run_requests(self.yaml_path, "register_api", tel=mobile, pwd=password)
        return res


